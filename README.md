California HR Services is a Human Resources consulting and outsourcing firm that specializes in a wide range of HR services. Our services include: recruitment, hiring, compliant employee handbooks, training, investigating meal & break violations, job descriptions and a plethora of other HR services.

Address: 1501 San Elijo Rd, Suite 104-101, San Marcos, CA 92078, USA

Phone: 858-228-5535

Website: https://www.cahrservices.com
